"use strict";

var App = angular.module("todo", ["ui.sortable", "LocalStorageModule"]);

App.controller("TodoCtrl", function ($scope, localStorageService) {

	$scope.init = function () {

		if (!localStorageService.get("todoList")) {
			$scope.model = [
						{ taskName: "All", isDone: false },
						{ taskName: "Farts", isDone: false },
            { taskName: "Given", isDone: false }
			];
		}else{
			$scope.model = localStorageService.get("todoList");
		}
    $scope.editable = false;
	};
  
  
  $scope.toggleUi = function(event){
    if(event.key == 'F8'){
      $scope.editable = !$scope.editable
    }
  };
  
	$scope.addTodo = function () {
		/*Should prepend to array*/
		$scope.model.push({taskName: $scope.newTodo, isDone: false });
		/*Reset the Field*/
		$scope.newTodo = "";
	};

	$scope.deleteTodo = function (item) {
		var index = $scope.model.indexOf(item);
		$scope.model.splice(index, 1);
	};

	$scope.todoSortable = {
		containment: "parent",//Dont let the user drag outside the parent
		cursor: "move",//Change the cursor icon on drag
		tolerance: "pointer"//Read http://api.jqueryui.com/sortable/#option-tolerance
	};

	$scope.$watch("model",function (newVal,oldVal) {
		if (newVal !== null && angular.isDefined(newVal) && newVal!==oldVal) {
			localStorageService.add("todoList",angular.toJson(newVal));
		}
	},true);

});